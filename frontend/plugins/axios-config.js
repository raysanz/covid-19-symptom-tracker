export default ({ app }) => {
    app.$axios.setBaseURL(process.env.API_URL);
};
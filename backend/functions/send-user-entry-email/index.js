const {
    response,
    mg,
    ddb
} = require('../common');

const { v4: uuid } = require('uuid');

exports.handler = async (event, context) => {
    console.log(event);
    const mailgun = await mg;

    const body = JSON.parse(event.body);

    const email = body.email;

    let token;

    try {
        let existingUser;

        const getExistingUser = {
            TableName: process.env.USER_TABLE,
            Key: {
                email: email
            }
        };

        existingUser = await ddb.get(getExistingUser).promise();

        if (!existingUser.Item) {
            const createUser = {
                TableName: process.env.USER_TABLE,
                Item: {
                    email: email,
                    createdDate: new Date().toISOString(),
                    notifications_enabled: 1,
                    token: uuid()
                }
            };

            await ddb.put(createUser).promise();

            token = createUser.Item.token;
        } else {
            token = existingUser.Item.token;
        }

        const result = await mailgun.messages.create('mg.bettercoding.dev', {
            from: 'COVID-19 Symptom Tracker <noreply@mg.bettercoding.dev>',
            to: email,
            subject: 'Get started with COVID-19 Symptom Tracker',
            html: `
                <h1>Welcome, and thank you for using the COVID-19 Symptom Tracker</h1>
                <p>
                    ${
                        existingUser.Item ? `
                            Great news, we found you in our database. Click <a href="http://localhost:3000/today?token=${token}">here<a/> to fill in today's update.
                        `
                        :
                        `
                            Thanks for signing up to track your symptoms. Click <a href="http://localhost:3000/today?token=${token}">here<a/> to fill in today's update.
                        `
                    }
                </p>
            `
        });

        console.log(result);

        const myResponse = response.ok('Email sent');

        console.log(myResponse);
        return myResponse;
    } catch (e) {
        console.error(e);
        return response.error(e);
    }
};
const {
    response,
    ddb,
    sqs
} = require('../common');

const moment = require('moment');
const bluebird = require('bluebird');

const queueForUser = (user, day) => {
    const request = {
        from: 'COVID-19 Symptom Tracker <noreply@mg.bettercoding.dev>',
        to: user.email,
        subject: `[${day}] Fill out your symptoms for today`,
        html: `
            <h1>It's time to fill out your symptoms for ${day}</h1>
            <p> Click <a href="http://localhost:3000/today?token=${user.token}">here<a/> to fill in today's update.</p>
        `
    };

    console.log(process.env.EMAIL_QUEUE_URL);

    return sqs.sendMessage({
        MessageBody: JSON.stringify(request),
        QueueUrl: process.env.EMAIL_QUEUE_URL
    }).promise();
};

exports.handler = async (event, context) => {
    console.log(event);

    const day = moment.utc().format('YYYY-MM-DD');

    console.log(process.env.USER_TABLE);

    try {
        const queryForUsersParam = {
            TableName: process.env.USER_TABLE,
            IndexName: 'NotificationsIndex',
            KeyConditionExpression: 'notifications_enabled = :notifications_enabled',
            ExpressionAttributeValues: {
                ':notifications_enabled': 1
            }
        }

        const users = await ddb.query(queryForUsersParam).promise();

        if (users.Items) {
            console.log(users.Items);

            await bluebird.map(users.Items, user => queueForUser(user, day));
        }

        const myResponse = response.ok()

        console.log(myResponse);
        return myResponse;
    } catch (e) {
        console.error(e);
        return response.error(e);
    }
};
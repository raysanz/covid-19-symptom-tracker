const {
    mg,
} = require('../common');

const bluebird = require('bluebird');

const sendEmail = (mailgun, content) => {
    return mailgun.messages.create('mg.bettercoding.dev', content);
};

exports.handler = async (event, context) => {
    console.log(event);

    const { Records } = event;

    const mailgun = await mg;

    await bluebird.map(Records, record => {
        const content = JSON.parse(record.body);

        return sendEmail(mailgun, content);
    });

    return {};
};
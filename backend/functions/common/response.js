const genericResponse = (code, body) => {
    const response = {
        statusCode: code,
        headers: {
            'Access-Control-Allow-Origin': '*',
        }
    };

    if (body) {
        response.body = typeof(body) === 'object' ? JSON.stringify(body) : body;
    }
    
    return response;
};

module.exports = {
    ok: (body) => {
        return genericResponse(200, body);
    },
    error: () => {
        return genericResponse(500);
    },
    notFound: () => {
        return genericResponse(404);
    },
    badRequest: (body) => {
        return genericResponse(400, body);
    }
};
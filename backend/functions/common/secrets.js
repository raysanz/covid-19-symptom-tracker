const AWS = require('aws-sdk');

const client = new AWS.SecretsManager({
    region: process.env.AWS_REGION
});

module.exports = client;
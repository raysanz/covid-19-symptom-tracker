const secretsManager = require('./secrets');

module.exports = (async () => {
    const secrets = JSON.parse((await secretsManager.getSecretValue({
        SecretId: process.env.COVID19_SECRETS
    }).promise()).SecretString);
    
    const mailgun = require('mailgun.js');

    return mailgun.client({
        username: 'api',
        key: secrets.MAILGUN_API_KEY || '7462012ec3d917573f178a566a197e51-2ae2c6f3-214ed41d'
    });
})();
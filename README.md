# COVID-19 Symptom Tracker

This workshop guides you through building a COVID-19 Daily symptom tracking service.
It uses:
* AWS Lambda
* API Gateway
* DynamoDB
* SQS
* Cloudwatch
* AWS SAM (Serverless Application Model)
* Nuxt.js
* Vue.js


## Dependencies
* Node.js >= 12 (https://nodejs.org/en/)
* Yarn (https://classic.yarnpkg.com/en/)
* AWS Sam (https://docs.aws.amazon.com/serverless-application-model/latest/developerguide/serverless-sam-cli-install.html)

## How to follow along
The workshop is broken up into steps (1 - 5).
The results of each step are in a branch named `step/x` where x is the step number.

### Step 1 (step/1)
Setting up AWS SAM, and Nuxt.js

### Step 2 (step/2)
Send user entry email (backend only)

### Step 3 (step/3)
Send user entry email (frontend)

### Step 4 (step/4)
Queue daily email for each user with notifications enabled (backend)

### Step 5 (step/5)
Send the daily email from SQS Queue (backend)

## Your tasks
1. Read through steps 1 - 5 and understand how they work
2. Checkout the step/5 branch and adjust all the variables (as per presentation) (I'll update this README later)
3. Implement the final part of this program, which is the ability to access the quiz on the url `/today?token={user token}`. And when the quiz is finished, submit the results to a new API gateway endpoint and lambda function, and then save it in the DynamoDB table.
